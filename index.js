/**
 * @format
 */

import { Navigation } from "react-native-navigation";
import App from './App';

//AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent("smartschool.WelcomeScreen", () => App);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "smartschool.WelcomeScreen"
      }
    }
  });
});
